import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:work_assignment/bottompage.dart';
import 'package:work_assignment/homepage.dart';
import 'package:work_assignment/loginPage.dart';
import 'package:work_assignment/mainpage.dart';
import 'package:work_assignment/test.dart';

void main() {
  // SystemChrome.setSystemUIOverlayStyle(
  //     SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: MainPage(),
  ));
}
