import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:work_assignment/bottompage.dart';
import 'homepage.dart';
import 'mainpage.dart';

class Loginpage extends StatefulWidget {
  @override
  _LoginpageState createState() => _LoginpageState();
}

Color textcolor = Colors.grey;
Icon checkIcon = Icon(
  Icons.check_sharp,
  color: Colors.white,
);

class _LoginpageState extends State<Loginpage> {
  static TextEditingController loginname_controller = TextEditingController();
  static TextEditingController password_controller = TextEditingController();
  @override
  void initState() {
    loginname_controller.text = 'davidwalb@gmail.com';
    password_controller.text = '12345678';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          VxAnimatedBox(
            child: Column(
              children: [
                Padding(padding: EdgeInsets.all(20)),
                section_head(),
                Padding(padding: EdgeInsets.all(80)),
                section_one(),
                section_two(),
                Padding(padding: EdgeInsets.all(10)),
                section_three()
              ],
            ),
          ).size(context.screenWidth, context.screenHeight).blueGray900.make(),
        ],
      ),
    );
  }

  Widget section_head() {
    return Container(
      margin: EdgeInsets.only(left: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Carrier',
            style: TextStyle(color: Colors.white),
          ),
          Padding(padding: EdgeInsets.only(left: 5)),
          Icon(
            Icons.wifi,
            color: Colors.white,
            size: 17,
          ),
          Padding(padding: EdgeInsets.only(left: 100)),
          Text(
            title,
            style: TextStyle(color: Colors.white),
          ),
          Padding(padding: EdgeInsets.only(left: 90)),
          Text(
            batterylevels.toString() + '%',
            style: TextStyle(color: Colors.white),
          ),
          Icon(
            Icons.battery_std,
            color: Colors.white,
            size: 17,
          )
        ],
      ),
    );
  }

  Widget section_one() {
    return Column(
      children: [
        Padding(padding: EdgeInsets.all(2)),
        Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/lplogo.jpg'), fit: BoxFit.fill),
                borderRadius: BorderRadius.circular(100),
                border: Border.all(width: 1, color: Colors.indigo)),
            height: 200,
            width: 200,
            child: Column(
              children: [
                Padding(padding: EdgeInsets.all(80)),
                Text(
                  'LIFE POINTS',
                  style: TextStyle(color: textcolor),
                )
              ],
            ))
      ],
    );
  }

  Widget section_two() {
    return Container(
        width: 300,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            textfield(labelText: 'EMAIL', field: loginname_controller),
            Padding(padding: EdgeInsets.all(8)),
            textfield(labelText: 'PASSWORD', field: password_controller),
            Padding(padding: EdgeInsets.all(5)),
            TextButton(
                onPressed: () {},
                child: Text('Forgot Password?',
                    style: TextStyle(color: textcolor))),
            Padding(padding: EdgeInsets.all(15)),
          ],
        ));
  }

  Widget section_three() {
    return Column(
      children: [
        FloatingActionButton.extended(
          heroTag: 1,
          label: Container(
            alignment: Alignment.center,
            height: 45,
            width: 300,
            child: Text(
              'Login',
              style: TextStyle(color: Colors.white),
            ),
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(30))),
          onPressed: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => Bottombar()));
          },
          backgroundColor: Colors.blue,
        ),
        Padding(padding: EdgeInsets.all(10)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Dont have an account?',
              style: TextStyle(color: Colors.white),
            ),
            TextButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => RegistratinPage()));
                },
                child: Text(
                  'Create One',
                  style: TextStyle(color: Colors.white),
                ))
          ],
        )
      ],
    );
  }

  Widget textfield({var labelText, var field}) {
    return TextField(
      style: TextStyle(color: Colors.white),
      obscureText: labelText == 'PASSWORD'
          ? true
          : false || labelText == 'CONFIRM PASSWORD'
              ? true
              : false,
      controller: field,
      decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.indigo)),
          labelText: labelText,
          labelStyle: TextStyle(color: textcolor, fontSize: 10),
          suffixIcon: labelText == 'CONFIRM PASSWORD'
              ? Icon(
                  Icons.cancel,
                  color: Colors.white,
                )
              : checkIcon),
    );
  }
}

class RegistratinPage extends StatefulWidget {
  @override
  _RegistratinPageState createState() => _RegistratinPageState();
}

TextEditingController regUsername_controller = TextEditingController();
TextEditingController regEmail_controller = TextEditingController();
TextEditingController regPassword_controller = TextEditingController();
TextEditingController regConfirmPassword_controller = TextEditingController();
var username = regUsername_controller.text;

class _RegistratinPageState extends State<RegistratinPage> {
  var value;
  Color floatingColor = Colors.blue;
  @override
  void initState() {
    regUsername_controller.text = 'DavidW';
    regEmail_controller.text = 'regUsername_controller';
    regPassword_controller.text = '12345678';
    regConfirmPassword_controller.text = '12345678';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          VxAnimatedBox(
            child: Column(
              children: [
                Padding(padding: EdgeInsets.all(20)),
                _LoginpageState().section_head(),
                Padding(padding: EdgeInsets.all(40)),
                section_one(),
                Padding(padding: EdgeInsets.all(15)),
                section_two(),
                section_three(),
                Padding(padding: EdgeInsets.all(20)),
                section_four()
              ],
            ),
          ).size(context.screenWidth, context.screenHeight).blueGray900.make(),
        ],
      ),
    );
  }

  Widget section_one() {
    return Column(
      children: [
        Text(
          'PROFILE PICTURE',
          style: TextStyle(color: textcolor),
        ),
        Padding(padding: EdgeInsets.all(2)),
        Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              border: Border.all(width: 1, color: Colors.indigo)),
          height: 70,
          width: 70,
          child: Center(
            child: Icon(
              Icons.photo_camera_sharp,
              color: Colors.white,
            ),
          ),
        )
      ],
    );
  }

  Widget section_two() {
    return Column(
      children: [
        Container(
            margin: EdgeInsets.only(left: 70),
            width: 300,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'GENDER',
                  style: TextStyle(color: textcolor),
                ),
                Padding(padding: EdgeInsets.all(5)),
                Row(
                  children: [
                    FloatingActionButton(
                      onPressed: () {
                        value = 0;
                        setState(() {});
                      },
                      child: Icon(
                        Icons.person,
                        // size: 10,
                        color: Colors.white,
                      ),
                      backgroundColor:
                          value == 0 ? floatingColor : Vx.blueGray800,
                    ),
                    Padding(padding: EdgeInsets.all(13)),
                    FloatingActionButton(
                      onPressed: () {
                        setState(() {});
                        value = 1;
                      },
                      child: Icon(
                        Icons.female_rounded,
                        color: Colors.white,
                      ),
                      backgroundColor:
                          value == 1 ? floatingColor : Vx.blueGray800,
                    ),
                    Padding(padding: EdgeInsets.all(13)),
                    FloatingActionButton(
                      onPressed: () {
                        value = 2;
                        setState(() {});
                      },
                      child: Icon(
                        Icons.person,
                        color: Colors.white,
                      ),
                      backgroundColor:
                          value == 2 ? floatingColor : Vx.blueGray800,
                    ),
                    Padding(padding: EdgeInsets.all(10)),
                    checkIcon
                  ],
                ),
                Padding(padding: EdgeInsets.all(1)),
                Row(
                  children: [
                    Padding(padding: EdgeInsets.all(5)),
                    Text('MALE', style: TextStyle(color: textcolor)),
                    Padding(padding: EdgeInsets.all(19)),
                    Text('FEMALE', style: TextStyle(color: textcolor)),
                    Padding(padding: EdgeInsets.all(16)),
                    Text('OTHER', style: TextStyle(color: textcolor))
                  ],
                )
              ],
            ))
      ],
    );
  }

  Widget section_three() {
    return Container(
        margin: EdgeInsets.only(left: 25),
        width: 300,
        child: Column(
          children: [
            _LoginpageState().textfield(
                labelText: 'USERNAME', field: regUsername_controller),
            Padding(padding: EdgeInsets.all(5)),
            _LoginpageState()
                .textfield(labelText: 'EMAIL', field: regEmail_controller),
            Padding(padding: EdgeInsets.all(5)),
            _LoginpageState().textfield(
                labelText: 'PASSWORD', field: regPassword_controller),
            Padding(padding: EdgeInsets.all(5)),
            _LoginpageState().textfield(
                labelText: 'CONFIRM PASSWORD',
                field: regConfirmPassword_controller),
          ],
        ));
  }

  Widget section_four() {
    return Container(
      margin: EdgeInsets.only(left: 25),
      child: Column(
        children: [
          FlatButton(
            height: 45,
            minWidth: 300,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(30))),
            onPressed: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => Verification()));
            },
            child: Text(
              'CREATE ACCOUNT',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blue,
          ),
          Padding(padding: EdgeInsets.all(10)),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Have have an account?',
                style: TextStyle(color: Colors.white),
              ),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => Loginpage()));
                  },
                  child: Text(
                    'Log In',
                    style: TextStyle(color: Colors.white),
                  ))
            ],
          )
        ],
      ),
    );
  }
}
