import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:battery/battery.dart';
import 'dart:async';
import 'dart:ffi';

import 'loginPage.dart';

var user;

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

Battery _battery = Battery();
var batterylevels;
var title;

class _MainPageState extends State<MainPage> {
  @override
  void initState() {
    user = 'DavidW';
    var date = DateTime.now().toString();
    var time1 = DateTime.parse(date);
    title = '${time1.hour}:${time1.minute}';
    getlevel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          VxAnimatedBox(
            child: Column(
              children: [
                Padding(padding: EdgeInsets.all(20)),
                section_head(),
                Padding(padding: EdgeInsets.all(100)),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => Loginpage()));
                  },
                  child: Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/lplogo.jpg'),
                              fit: BoxFit.fill),
                          borderRadius: BorderRadius.circular(200),
                          border: Border.all(width: 1, color: Colors.indigo)),
                      height: 300,
                      width: 300,
                      child: Column(
                        children: [
                          Padding(padding: EdgeInsets.all(120)),
                          Text(
                            'LIFE POINTS',
                            style: TextStyle(color: textcolor),
                          )
                        ],
                      )),
                )
              ],
            ),
          ).size(context.screenWidth, context.screenHeight).blueGray900.make(),
        ],
      ),
    );
  }

  Widget section_head() {
    return Container(
      margin: EdgeInsets.only(left: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Carrier',
            style: TextStyle(color: Colors.white),
          ),
          Padding(padding: EdgeInsets.only(left: 5)),
          Icon(
            Icons.wifi,
            color: Colors.white,
            size: 17,
          ),
          Padding(padding: EdgeInsets.only(left: 100)),
          Text(
            title,
            style: TextStyle(color: Colors.white),
          ),
          Padding(padding: EdgeInsets.only(left: 90)),
          Text(
            batterylevels.toString() + '%',
            style: TextStyle(color: Colors.white),
          ),
          Icon(
            Icons.battery_std,
            color: Colors.white,
            size: 17,
          )
        ],
      ),
    );
  }

  getlevel() async {
    final int batterylevel = await _battery.batteryLevel;
    setState(() {
      batterylevels = batterylevel;
    });
  }
}
