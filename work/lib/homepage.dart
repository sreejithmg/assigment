import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:work_assignment/bottompage.dart';
import 'package:work_assignment/loginPage.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'mainpage.dart';
import 'model/usermodel.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

var cartlist = [];

class _HomePageState extends State<HomePage> {
  List shoeList = [];

  @override
  void initState() {
    shoeListadd();
    cartListAdd();
    super.initState();
  }

  shoeListadd() {
    shoeList.add(ShoeModel(
        name: 'NIKE',
        image: 'assets/nike.jpg',
        head: 'NIKE JOYRIDE',
        desc:
            'Nike Air technology consists of pressurized air inside a tough yet flexible bag and provides more flexibility and spring without compromising structure'));
    shoeList.add(ShoeModel(
        name: 'ADDIDAS',
        image: 'assets/addidas.jpg',
        head: 'ADDIDAS JOYRIDE',
        desc:
            'Addidas Air technology consists of pressurized air inside a tough yet flexible bag and provides more flexibility and spring without compromising structure'));
    shoeList.add(ShoeModel(
        name: 'PUMA',
        image: 'assets/puma.jpg',
        head: 'PUMA JOYRIDE',
        desc:
            'Puma technology consists of pressurized air inside a tough yet flexible bag and provides more flexibility and spring without compromising structure'));
  }

  cartListAdd() {
    cartlist.add(CartImageModel(image: 'assets/Mc.jpg', points: '200'));
    cartlist.add(CartImageModel(image: 'assets/iphone.jpg', points: '500'));
    cartlist.add(CartImageModel(image: 'assets/addidas.jpg', points: '300'));
    cartlist.add(CartImageModel(image: 'assets/spotify.jpg', points: '100'));
    cartlist.add(CartImageModel(image: 'assets/netflix.jpg', points: '200'));
    cartlist.add(CartImageModel(image: 'assets/amazone.jpg', points: '200'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          VxAnimatedBox(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                section_one(),
                Padding(padding: EdgeInsets.all(10)),
                Container(
                  child: Text(
                    'NEWS',
                    style: TextStyle(color: Colors.white),
                  ),
                  margin: EdgeInsets.only(left: 50),
                ),
                VxSwiper.builder(
                    enlargeCenterPage: true,
                    aspectRatio: 1,
                    itemCount: shoeList.length,
                    itemBuilder: (context, index) {
                      return VxBox(
                              child: ZStack([
                        Align(
                          alignment: Alignment.topCenter,
                          child: Text(
                            (shoeList[index] as ShoeModel).head,
                            style: TextStyle(color: Colors.white, fontSize: 15),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: VStack([
                            Container(
                                decoration: BoxDecoration(
                                  color: Vx.blueGray800,
                                  borderRadius: BorderRadius.circular(9),
                                ),
                                width: 270,
                                height: 50,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      (shoeList[index] as ShoeModel).name,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    Text(
                                      (shoeList[index] as ShoeModel).desc,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 7),
                                    ),
                                  ],
                                ))
                          ]),
                        )
                      ]))
                          .height(500)
                          .bgImage(DecorationImage(
                              image: AssetImage(
                                  (shoeList[index] as ShoeModel).image),
                              fit: BoxFit.cover,
                              colorFilter: ColorFilter.mode(
                                  Colors.black.withOpacity(0.3),
                                  BlendMode.darken)))
                          .withRounded(value: 10)
                          .make()
                          .p12()
                          .centered();
                    })
              ],
            ),
          ).size(context.screenWidth, context.screenHeight).blueGray900.make()
        ],
        fit: StackFit.expand,
      ),
    );
  }

  Widget section_head() {
    return Container(
      margin: EdgeInsets.only(left: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Carrier',
            style: TextStyle(color: Colors.white),
          ),
          Padding(padding: EdgeInsets.only(left: 5)),
          Icon(
            Icons.wifi,
            color: Colors.white,
            size: 17,
          ),
          Padding(padding: EdgeInsets.only(left: 100)),
          Text(
            title,
            style: TextStyle(color: Colors.white),
          ),
          Padding(padding: EdgeInsets.only(left: 90)),
          Text(
            batterylevels.toString() + '%',
            style: TextStyle(color: Colors.white),
          ),
          Icon(
            Icons.battery_std,
            color: Colors.white,
            size: 17,
          )
        ],
      ),
    );
  }

  section_one() {
    return Container(
      child: Column(
        children: [
          Padding(padding: EdgeInsets.only(top: 35)),
          _HomePageState().section_head(),
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(top: 25, left: 10),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/david.jpg'),
                        fit: BoxFit.fill),
                    borderRadius: BorderRadius.circular(100),
                    border: Border.all(width: 1, color: Colors.indigo)),
                height: 50,
                width: 50,
              ),
              Padding(padding: EdgeInsets.only(left: 30)),
              Column(
                children: [
                  Padding(padding: EdgeInsets.only(top: 30)),
                  Text(
                    user,
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
              Padding(padding: EdgeInsets.only(left: 100, top: 10)),
              Column(
                children: [
                  Padding(padding: EdgeInsets.only(top: 35)),
                  Text('POINTS', style: TextStyle(color: Colors.white)),
                  Padding(padding: EdgeInsets.all(5)),
                  Text('40',
                      style: TextStyle(color: Colors.white, fontSize: 30))
                ],
              ),
              Padding(padding: EdgeInsets.only(left: 30)),
              Column(
                children: [
                  Padding(padding: EdgeInsets.only(top: 35)),
                  Stack(
                    children: [
                      Container(
                        child: IconButton(
                            onPressed: () {},
                            icon: Text('LF',
                                style: TextStyle(
                                    color: Colors.blue,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold))),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            border: Border.all(width: 1, color: Colors.indigo)),
                        height: 50,
                        width: 50,
                      ),
                      Row(
                        children: [
                          SizedBox(
                              height: 50,
                              width: 50,
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.black,
                                value: 0.4,
                                strokeWidth: 4,
                              ))
                        ],
                      )
                    ],
                  )
                ],
              ),
            ],
          )
        ],
      ),
      height: 150,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: [Vx.purple800, Vx.indigo900])),
    );
  }
}

class Digitalcard extends StatefulWidget {
  @override
  _DigitalcardState createState() => _DigitalcardState();
}

class _DigitalcardState extends State<Digitalcard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          VxAnimatedBox(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _HomePageState().section_one(),
                Padding(padding: EdgeInsets.all(10)),
                Container(
                  child: Text(
                    'DIGITAl MEMBER CARD',
                    style: TextStyle(color: Colors.white),
                  ),
                  margin: EdgeInsets.only(left: 50),
                ),
                Padding(padding: EdgeInsets.all(50)),
                section_two()
              ],
            ),
          ).size(context.screenWidth, context.screenHeight).blueGray900.make()
        ],
        fit: StackFit.expand,
      ),
    );
  }

  Widget section_two() {
    return Container(
      margin: EdgeInsets.all(80),
      height: 200,
      width: 200,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(width: 7, color: Colors.grey),
          image: DecorationImage(
              image: AssetImage('assets/qr.jpg'), fit: BoxFit.fill)),
    );
  }
}

class RankOrder extends StatefulWidget {
  @override
  _RankOrderState createState() => _RankOrderState();
}

class _RankOrderState extends State<RankOrder> {
  int i = 0;
  set value(bool value) {}
  var mp;
  var map;
  List userlist = [];
  @override
  void initState() {
    getuserlist();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          VxAnimatedBox(
            child: Column(
              children: [
                _HomePageState().section_one(),
                Padding(padding: EdgeInsets.all(10)),
                section_one(),
                Padding(padding: EdgeInsets.all(15)),
                section_two(),
                Padding(padding: EdgeInsets.all(50)),
              ],
            ),
          ).size(context.screenWidth, context.screenHeight).blueGray900.make()
        ],
        fit: StackFit.expand,
      ),
    );
  }

  Widget section_one() {
    bool value = true;
    return Container(
      margin: EdgeInsets.only(left: 50),
      child: Row(
        children: [
          Column(
            children: [
              Text(
                'RANKING',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
          Padding(padding: EdgeInsets.only(left: 80)),
          Text(
            'Monthly',
            style: TextStyle(color: Colors.white),
          ),
          Switch.adaptive(value: true, onChanged: (value) {}),
          Text(
            'Yearly',
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );
  }

  Widget section_two() {
    return Expanded(
      child: ListView.separated(
          itemBuilder: (context, index) {
            return ListTile(
                trailing: IconButton(
                    onPressed: () {},
                    icon: Text('LF${(userlist[index] as UserModel).rank}',
                        style: TextStyle(
                            color: (userlist[index] as UserModel).rank == '1' ||
                                    (userlist[index] as UserModel).rank == '2'
                                ? Colors.yellow[700]
                                : (userlist[index] as UserModel).rank == '3'
                                    ? Colors.orange[700]
                                    : (userlist[index] as UserModel).rank == '4'
                                        ? Colors.purple[700]
                                        : Colors.red[700],
                            fontSize: 13,
                            fontWeight: FontWeight.bold))),
                leading: Text((userlist[index] as UserModel).rank,
                    style: TextStyle(color: Colors.white)),
                title: Row(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(
                                  (userlist[index] as UserModel).image),
                              fit: BoxFit.fill),
                          borderRadius: BorderRadius.circular(100),
                          border: Border.all(width: 1, color: Colors.indigo)),
                      height: 40,
                      width: 40,
                    ),
                    Padding(padding: EdgeInsets.only(left: 15)),
                    Text((userlist[index] as UserModel).name,
                        style: TextStyle(color: Colors.white)),
                  ],
                ),
                subtitle: Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 55),
                      child: Icon(
                        Icons.star,
                        size: 10,
                        color: Colors.white,
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(left: 5)),
                    Container(
                      child: Text((userlist[index] as UserModel).points,
                          style: TextStyle(color: Colors.white, fontSize: 10)),
                    )
                  ],
                ));
          },
          separatorBuilder: (context, index) => Divider(
                height: 30,
                color: Colors.black,
              ),
          itemCount: userlist.length),
    );
  }

  getuserlist() async {
    if (await UserDatabase().userDatabase() != null) {
      for (var admin in await UserDatabase().getUserData()) {
        userlist.add(admin);
        setState(() {});
      }
    }
  }
}

class Verification extends StatefulWidget {
  @override
  _VerificationState createState() => _VerificationState();
}

class _VerificationState extends State<Verification> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          VxAnimatedBox(
            child: Column(
              //crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _HomePageState().section_one(),
                Padding(padding: EdgeInsets.all(10)),
                section_one(),
                Padding(padding: EdgeInsets.all(10)),
                section_two(),
                Padding(padding: EdgeInsets.only(top: 10)),
                section_three(),
              ],
            ),
          ).size(context.screenWidth, context.screenHeight).blueGray900.make()
        ],
        fit: StackFit.expand,
      ),
    );
  }

  Widget section_one() {
    return Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/lplogo.jpg'), fit: BoxFit.fill),
            borderRadius: BorderRadius.circular(100),
            border: Border.all(width: 1, color: Colors.indigo)),
        height: 120,
        width: 120,
        child: Column(
          children: [
            Padding(padding: EdgeInsets.all(45)),
            Text(
              'LIFE POINTS',
              style: TextStyle(color: textcolor, fontSize: 10),
            )
          ],
        ));
  }

  Widget section_two() {
    return Column(
      children: [
        Row(
          children: [
            Padding(padding: EdgeInsets.only(left: 35)),
            Icon(
              Icons.arrow_back,
              color: Colors.white,
              size: 30,
            ),
            Padding(padding: EdgeInsets.only(left: 10)),
            Text(
              'Verification',
              style: TextStyle(color: Colors.white, fontSize: 25),
            )
          ],
        ),
        Padding(
            padding: EdgeInsets.only(top: 30, left: 10),
            child: Text(
              'We have sent you a verification code to your email ID',
              style: TextStyle(color: Colors.white, fontSize: 12),
            )),
        Padding(
          padding: EdgeInsets.only(top: 15, right: 150),
          child: Text(
            'davidwalb@gmail.com',
            style: TextStyle(color: Colors.white, fontSize: 12),
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 50, vertical: 50),
          child: PinFieldAutoFill(
            decoration: UnderlineDecoration(
                colorBuilder: FixedColorBuilder(Colors.indigo),
                textStyle: TextStyle(color: Colors.white, fontSize: 25)),
            codeLength: 4,
            onCodeChanged: (val) {
              print(val);
            },
          ),
        ),
      ],
    );
  }

  Widget section_three() {
    return Column(
      children: [
        Text('Didnt get a code? Tap to resend',
            style: TextStyle(color: Colors.white, fontSize: 15)),
        Padding(padding: EdgeInsets.only(top: 90)),
        FloatingActionButton.extended(
          heroTag: 1,
          label: Container(
            alignment: Alignment.center,
            width: 250,
            child: Text(
              'VERIFY',
              style: TextStyle(color: Colors.white),
            ),
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(30))),
          onPressed: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => Bottombar()));
          },
          backgroundColor: Colors.blue,
        ),
        Padding(padding: EdgeInsets.only(top: 20)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Have an account?', style: TextStyle(color: Colors.white)),
            TextButton(
                onPressed: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => Loginpage()));
                },
                child: Text('Login', style: TextStyle(color: Colors.white)))
          ],
        )
      ],
    );
  }
}

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  @override
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          VxAnimatedBox(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _HomePageState().section_one(),
                Padding(padding: EdgeInsets.all(10)),
                Container(
                  child: Text(
                    'SHOP',
                    style: TextStyle(color: Colors.white),
                  ),
                  margin: EdgeInsets.only(left: 50),
                ),
                Padding(padding: EdgeInsets.only(top: 10)),
                section_one()
              ],
            ),
          ).size(context.screenWidth, context.screenHeight).blueGray900.make()
        ],
        fit: StackFit.expand,
      ),
    );
  }

  Widget section_one() {
    return Container(
        child: Expanded(
      child: GestureDetector(
          onTap: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => AddidaPage()));
          },
          child: Stack(children: [
            StaggeredGridView.countBuilder(
              crossAxisCount: 4,
              itemCount: cartlist.length,
              itemBuilder: (BuildContext context, int index) => new Container(
                height: 120.0,
                width: 120.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image:
                        AssetImage((cartlist[index] as CartImageModel).image),
                    fit: BoxFit.fill,
                  ),
                  shape: BoxShape.rectangle,
                ),
              ),
              staggeredTileBuilder: (int index) =>
                  new StaggeredTile.count(2, index.isEven ? 3 : 2),
              mainAxisSpacing: 4.0,
              crossAxisSpacing: 4.0,
            )
          ])),
    ));
  }
}

class AddidaPage extends StatefulWidget {
  @override
  _AddidaPageState createState() => _AddidaPageState();
}

class _AddidaPageState extends State<AddidaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          VxAnimatedBox(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _HomePageState().section_one(),
                Padding(padding: EdgeInsets.all(10)),
                Container(
                  child: Text(
                    'SHOP',
                    style: TextStyle(color: Colors.white),
                  ),
                  margin: EdgeInsets.only(left: 50),
                ),
                section_one()
              ],
            ),
          ).size(context.screenWidth, context.screenHeight).blueGray900.make()
        ],
        fit: StackFit.expand,
      ),
    );
  }

  Widget section_one() {
    return Column(children: [
      Container(
          margin: EdgeInsets.all(10),
          width: 300,
          height: 500,
          child: Column(
            children: [
              Container(
                  margin: EdgeInsets.only(left: 40),
                  height: 500,
                  child: Card(
                      margin: const EdgeInsets.all(10.0),
                      color: Vx.blueGray800,
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(20),
                        topLeft: Radius.circular(20),
                      )),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                            ),
                            child: Image(
                              fit: BoxFit.fill,
                              image: AssetImage('assets/addidas.jpg'),
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(top: 30)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text('Adidas 10% Off',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20)),
                              Padding(padding: EdgeInsets.only(left: 20)),
                              Column(
                                children: [
                                  Text('300',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20)),
                                  Text('POINTS',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 10))
                                ],
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 20, left: 50)),
                          Text(
                              'Get 10% Off of a   €100 purchase.Go to your nearest',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 10)),
                          Text(
                              'store to avail this discount.Valid from Jun-July,2020',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 10)),
                          Padding(padding: EdgeInsets.only(top: 40)),
                          FloatingActionButton.extended(
                            heroTag: 1,
                            label: Container(
                              alignment: Alignment.center,
                              width: 150,
                              child: Text(
                                'BUY NOW',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(30))),
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => BuyPageOne()));
                            },
                            backgroundColor: Colors.blue,
                          ),
                        ],
                      )))
            ],
          ))
    ]);
  }
}

class BuyPageOne extends StatefulWidget {
  @override
  _BuyPageOneState createState() => _BuyPageOneState();
}

class _BuyPageOneState extends State<BuyPageOne> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          VxAnimatedBox(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _HomePageState().section_one(),
                Padding(padding: EdgeInsets.all(10)),
                Container(
                    margin: EdgeInsets.only(left: 15),
                    child: Row(
                      children: [
                        IconButton(
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => CartPage()));
                          },
                          icon: Icon(Icons.arrow_back),
                          color: Colors.white,
                        ),
                        Text(
                          'SHOP',
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    )),
                section_one()
              ],
            ),
          ).size(context.screenWidth, context.screenHeight).blueGray900.make()
        ],
        fit: StackFit.expand,
      ),
    );
  }

  Widget section_one() {
    return Container(
        alignment: Alignment.center,
        child: Padding(
            padding: EdgeInsets.only(left: 20),
            child: Column(
              children: [
                Padding(padding: EdgeInsets.only(left: 40, top: 40)),
                Container(
                  height: 100,
                  width: 300,
                  decoration: BoxDecoration(
                      gradient:
                          LinearGradient(colors: [Vx.blue900, Vx.purple900]),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      )),
                  child: Row(
                    children: [
                      Column(
                        children: [
                          Padding(padding: EdgeInsets.all(10)),
                          Text('Adidas 10% Off',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20)),
                          Padding(
                            padding: EdgeInsets.only(right: 70),
                            child: Text(
                              'Jun-July,2020',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 10),
                            ),
                          ),
                        ],
                      ),
                      Spacer(),
                      Column(
                        children: [
                          Padding(padding: EdgeInsets.all(10)),
                          Text('300',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20)),
                          Text('POINTS',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 10))
                        ],
                      )
                    ],
                  ),
                ),
                Container(
                    color: Colors.indigo[900],
                    width: 300,
                    height: 400,
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                    padding:
                                        EdgeInsets.only(top: 10, left: 20)),
                                Text(
                                  'FIRST NAME',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 10),
                                ),
                                Container(
                                    width: 100,
                                    child: textfield(hintText: 'David')),
                              ],
                            ),
                            Spacer(),
                            Column(
                              children: [
                                Padding(padding: EdgeInsets.only(top: 10)),
                                Text(
                                  'LAST NAME',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 10),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 45),
                                  width: 100,
                                  child: textfield(hintText: 'Walberg'),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Padding(
                            padding: EdgeInsets.only(right: 220, top: 10),
                            child: Text(
                              'PHONE NUMBER',
                              style: TextStyle(
                                  color: Colors.white54, fontSize: 10),
                            )),
                        textfield(hintText: '+49 1515557847', fontSize: 12.0),
                        Padding(
                            padding: EdgeInsets.only(right: 250, top: 10),
                            child: Text(
                              'ADDRESS',
                              style: TextStyle(
                                  color: Colors.white54, fontSize: 10),
                            )),
                        textfield(
                            hintText: 'Boxhagener Str. 13', fontSize: 12.0),
                        textfield(
                            hintText: 'Hamburg Stellingen,Hamburg(HH),22525',
                            fontSize: 12.0),
                        Padding(padding: EdgeInsets.only(top: 50)),
                        FloatingActionButton.extended(
                          heroTag: 1,
                          label: Container(
                            alignment: Alignment.center,
                            width: 200,
                            child: Text(
                              'BUY NOW',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30))),
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => BuyPageTwo()));
                          },
                          backgroundColor: Colors.blue,
                        ),
                      ],
                    ))
              ],
            )));
  }

  Widget textfield({var hintText, var fontSize}) {
    return TextField(
      enabled: false,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: TextStyle(color: Colors.white, fontSize: fontSize),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.indigo),
        ),
      ),
    );
  }
}

class BuyPageTwo extends StatefulWidget {
  @override
  _BuyPageTwoState createState() => _BuyPageTwoState();
}

class _BuyPageTwoState extends State<BuyPageTwo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          VxAnimatedBox(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _HomePageState().section_one(),
                Padding(padding: EdgeInsets.all(10)),
                Container(
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => CartPage()));
                        },
                        icon: Icon(Icons.arrow_back),
                        color: Colors.white,
                      ),
                      Text(
                        'SHOP',
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  margin: EdgeInsets.only(left: 15),
                ),
                Padding(padding: EdgeInsets.only(top: 10)),
                section_one()
              ],
            ),
          ).size(context.screenWidth, context.screenHeight).blueGray900.make()
        ],
        fit: StackFit.expand,
      ),
    );
  }

  Widget section_one() {
    return Container(
      alignment: Alignment.center,
      child: Container(
        width: 300,
        height: 500,
        decoration: BoxDecoration(
            color: Vx.blueGray800,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              bottomRight: Radius.circular(20),
            )),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            Icon(
              Icons.check_circle_outline,
              color: Colors.white,
            ),
            Padding(padding: EdgeInsets.only(top: 20)),
            Text(
              'Thank you for your purchase!',
              style: TextStyle(color: Colors.white),
            ),
            TextField(
              decoration: InputDecoration(
                hintStyle: TextStyle(color: Colors.white, fontSize: 12),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 20)),
            Row(
              children: [
                Padding(padding: EdgeInsets.only(left: 20)),
                Column(
                  children: [
                    Text('Adidas 10% Off',
                        style: TextStyle(color: Colors.white, fontSize: 20)),
                    Text('Jun-July,2020',
                        style: TextStyle(color: Colors.white, fontSize: 10)),
                  ],
                ),
                Padding(padding: EdgeInsets.only(left: 85)),
                Padding(padding: EdgeInsets.only(top: 20)),
                Column(
                  children: [
                    Text('300',
                        style: TextStyle(color: Colors.white, fontSize: 20)),
                    Text('POINTS',
                        style: TextStyle(color: Colors.white, fontSize: 10))
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
