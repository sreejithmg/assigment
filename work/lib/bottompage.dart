import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:work_assignment/homepage.dart';

class Bottombar extends StatefulWidget {
  @override
  _BottombarState createState() => _BottombarState();
}

final tabs = [HomePage(), Digitalcard(), RankOrder(), CartPage()];
int currentindex = 0;

class _BottombarState extends State<Bottombar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: tabs[currentindex],
      bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          onTap: (index) {
            setState(() {
              print(index);
              currentindex = index;
            });
          },
          currentIndex: currentindex,
          selectedItemColor: Colors.red[900],
          backgroundColor: Vx.blueGray900,
          items: [
            bottomButton(Icon(
              Icons.menu,
            )),
            bottomButton(Icon(
              Icons.crop_square,
            )),
            bottomButton(Icon(
              Icons.stacked_bar_chart,
            )),
            bottomButton(Icon(
              Icons.shopping_cart,
            )),
          ]),
    );
  }

  bottomButton(Icon icon) {
    return BottomNavigationBarItem(
      icon: icon,
      title: Text(''),
    );
  }
}
