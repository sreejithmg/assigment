import 'dart:convert';

import 'package:firebase_database/firebase_database.dart';

class UserModel {
  var key;
  var name;
  var points;
  var rank;
  var image;
  UserModel({
    this.key,
    this.name,
    this.points,
    this.rank,
    this.image,
  });
  Map<String, dynamic> tomap() {
    return {'NAME': name, 'POINTS': points, 'RANK': rank, 'IMAGE': image};
  }
}

class UserDatabase {
  DatabaseReference userDatabase() {
    return FirebaseDatabase.instance.reference().child('User Database');
  }

  sendUserData(UserModel user) {
    return userDatabase().push().set(user.tomap());
  }

  Future<List<UserModel>> getUserData() async {
    var userList;
    var db = await userDatabase().once();
    Map<dynamic, dynamic> user = db.value;
    print(user);
    if (user != null) {
      List<UserModel> users = [];
      user.forEach((key, value) {
        users.add(UserModel(
            key: key,
            name: value['NAME'],
            points: value['POINTS'],
            rank: value['RANK'],
            image: value['IMAGE']));
      });
      userList = users;
      return users;
    }
    return userList;
  }
}

class ShoeModel {
  var name;
  var image;
  var desc;
  var head;
  ShoeModel({this.name, this.image, this.desc, this.head});
  Map<String, dynamic> tomap() {
    return {'NAME': name, 'IMAGE': image, 'DESC': desc, 'HEAD': head};
  }
}

class CartImageModel {
  var name;
  var image;
  var points;
  CartImageModel({this.name, this.image, this.points});
  Map<String, dynamic> tomap() {
    return {'NAME': name, 'IMAGE': image, 'POINTS': points};
  }
}
